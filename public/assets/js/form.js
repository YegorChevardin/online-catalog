$(document).ready(function() {
    $(".catalog-form").submit(function() { //Change
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "/post", //Change
            data: th.serialize()
        }).done(function() {
            setTimeout(function() {
                $('.thank-you-alert-button').click();
            }, 1000);
        });
        return false;
    });
});
