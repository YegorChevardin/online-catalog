/* popup form section start */
$('#eco-popup').magnificPopup({
    items: {
        src: '#catalog-form'
    }
});

$('#soc-popup').magnificPopup({
   items: {
       src: '#catalog-form'
   }
});

$('.thank-you-alert-button').magnificPopup({
    items: {
        src: '.thank-you-alert-box'
    }
});
/* popup form section end */

/*polygon section start*/
var divs = document.getElementsByClassName('collapse'),
    buttons = document.getElementsByClassName('btn-link'),
    polygons = document.getElementsByClassName('polygon-side');

for(let id in divs) {
    let div = divs.item(id),
        button = buttons.item(id),
        polygon = polygons.item(id);

    if(id == "length" || id == "item" || id == "namedItem") {
        continue;
    } else {
        button.addEventListener('click', function() {
            for(let element in polygons) {
                if(element == id || element == "length" || element == "item" || element == "namedItem") {
                    continue;
                } else {
                    polygons.item(element).style.transform = "rotate(0deg)";
                }
            }

            if(div.classList.contains('show')) {
                polygon.style.transform = "rotate(0deg)";
            } else {
                polygon.style.transform = "rotate(90deg)";
            }
        });
    }
}
/*polygon section end*/

/*filters section start*/
function sort_up(attribute) {
    let cards_parent = document.querySelector('#accordionExample');

    for(let i = 0; i < cards_parent.children.length; i++) {
        for(let j = i; j < cards_parent.children.length; j++) {
            if(+cards_parent.children[i].getAttribute(attribute) > +cards_parent.children[j].getAttribute(attribute)) {
                let replacedNode = cards_parent.replaceChild(cards_parent.children[j], cards_parent.children[i]);
                insertAfter(replacedNode, cards_parent.children[i]);
            }
        }
    }
}

function sort_down(attribute) {
    let cards_parent = document.querySelector('#accordionExample');

    for(let i = 0; i < cards_parent.children.length; i++) {
        for(let j = i; j < cards_parent.children.length; j++) {
            if(+cards_parent.children[i].getAttribute(attribute) < +cards_parent.children[j].getAttribute(attribute)) {
                let replacedNode = cards_parent.replaceChild(cards_parent.children[j], cards_parent.children[i]);
                insertAfter(replacedNode, cards_parent.children[i]);
            }
        }
    }
}

function sort_up_string(attribute) {
    let cards_parent = document.querySelector('#accordionExample');

    for(let i = 0; i < cards_parent.children.length; i++) {
        for(let j = i; j < cards_parent.children.length; j++) {
            if(+cards_parent.children[i].getAttribute(attribute).length > +cards_parent.children[j].getAttribute(attribute).length) {
                let replacedNode = cards_parent.replaceChild(cards_parent.children[j], cards_parent.children[i]);
                insertAfter(replacedNode, cards_parent.children[i]);
            }
        }
    }
}

function sort_down_string(attribute) {
    let cards_parent = document.querySelector('#accordionExample');

    for(let i = 0; i < cards_parent.children.length; i++) {
        for(let j = i; j < cards_parent.children.length; j++) {
            if(+cards_parent.children[i].getAttribute(attribute).length < +cards_parent.children[j].getAttribute(attribute).length) {
                let replacedNode = cards_parent.replaceChild(cards_parent.children[j], cards_parent.children[i]);
                insertAfter(replacedNode, cards_parent.children[i]);
            }
        }
    }
}

function insertAfter(elem, refelem) {
    return refelem.parentNode.insertBefore(elem, refelem.nextSibling);
}

var menu_buttons = document.getElementsByClassName('nav-item');
for (let menu_id in menu_buttons) {
    let menu_button = menu_buttons.item(menu_id),
    current_menu_polygon;

    if(menu_id == "item" || menu_id == "length" || menu_id == "namedItem") {
        continue;
    } else if(menu_id == 1) {
        menu_button.addEventListener('click', function() {
            current_menu_polygon = menu_button.children[0].children[1];

            if(current_menu_polygon.classList.contains("rotated")) {
                current_menu_polygon.style.transform = "rotate(0deg)";
                current_menu_polygon.classList.remove("rotated");
                sort_up('data-date');
            } else {
                current_menu_polygon.style.transform = "rotate(180deg)";
                current_menu_polygon.classList.add("rotated");
                sort_down('data-date');
            }
        });
    } else if(menu_id == 0){
        menu_button.addEventListener('click', function() {
            current_menu_polygon = menu_button.children[1];

            if(current_menu_polygon.classList.contains("rotated")) {
                current_menu_polygon.style.transform = "rotate(0deg)";
                current_menu_polygon.classList.remove("rotated");
                sort_up_string('data-name');
            } else {
                current_menu_polygon.style.transform = "rotate(180deg)";
                current_menu_polygon.classList.add("rotated");
                sort_down_string('data-name');
            }
        });
    } else if(menu_id == 2) {
        menu_button.addEventListener('click', function() {
            current_menu_polygon = menu_button.children[1];

            if(current_menu_polygon.classList.contains("rotated")) {
                current_menu_polygon.style.transform = "rotate(0deg)";
                current_menu_polygon.classList.remove("rotated");
                sort_up('data-budget');
            } else {
                current_menu_polygon.style.transform = "rotate(180deg)";
                current_menu_polygon.classList.add("rotated");
                sort_down('data-budget');
            }
        });
    }
}
/*filters section end*/
