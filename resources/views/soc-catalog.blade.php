@extends('layout.template')
@section('content')
    <div class="container">
        <!-- buttons section start -->
        <div class="row mt-2">
            <div class="col-md m-0 p-0 mr-2 ml-2 eco-col">
                <button type="button" class="btn btn-lg btn-block btn-secondary eco-btn w-100 h-100" onclick="window.location.href='/'">Каталог ЕКОсвідомого бізнесу</button>
            </div>
            <div class="col-md m-0 p-0 ml-2 mr-2 eco-col">
                <button type="button" class="btn btn-lg btn-block btn-primary eco-btn w-100 h-100" onclick=window.location.href="{{ route('soc') }}">Каталог СОЦсвідомого бізнесу</button>
            </div>
        </div>
        <!-- buttons section end -->
        <!-- banner section start -->
        <div class="row mt-3">
            <div class="col-md p-0 m-0 mr-2 ml-2 w-100">
                <div class="card overflow-hidden text-white">
                    <img src="{{asset('assets/img/banner/sociology-promo.png')}}" class="card-img">
                    <div class="card-img-overlay banner h-100">
                        <h5 class="card-title">Вклад бізнесу в соціальний розвіток</h5>
                        <p class="card-text">
                            Ми зібрали найкращі практики бізнесу, які допомагають бізнесу<br/>
                            турбуватися про суспільство
                        </p>
                        <button id="soc-popup" type="button" class="btn btn-success btn-large text-left add-case"><span>Додати кейс</span> <span>+</span></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner section end -->
        <!-- popup form section start -->
        <div id="catalog-form" class="position-relative mx-auto mfp-hide">
            <div class="popup-header w-100 text-center">
                <h2>
                    Додати кейс у онлайн-каталог<br/>
                    СОЦвідповідального бізнесу
                </h2>
            </div>
            <div class="popup-body w-100">
                <form class="catalog-form" action="{{ route('post') }}" method="POST" autocomplete="on">
                @csrf
                <!-- website input section start -->
                    <input type="hidden" name="form_subject" value="Soc post"/>
                    <!-- website input section end -->
                    <!-- user input section start -->
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputCompanyName">1. Назва компанії</label>
                                <input type="text" class="form-control" id="inputCompanyName" name="Company_name" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputProjectName">2. Назва проекту / ініціативи</label>
                                <input type="text" class="form-control" id="inputProjectName" name="Project_name" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputBudget">3. Бюджет проекту</label>
                                <input type="text" class="form-control" id="inputBudget" name="Budget" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputDescription">4. Опис проекту</label>
                                <textarea type="text" class="form-control" id="inputDescription" name="Description" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputLink">5. Посилання на джерело / публікації</label>
                                <textarea type="text" class="form-control" id="inputLink" name="Links" placeholder="Між посиланнями пробіл" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputName">6. Ваше ім'я та прізвище</label>
                                <input type="text" class="form-control" id="inputName" name="Full_name" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputName">7. Дата початку проекта</label>
                                <input type="date" class="form-control" id="inputStartDate" name="Start_date" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="inputName">8. Дата закінчення проекта</label>
                                <input type="date" class="form-control" id="inputEndDate" name="End_date" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md m-0 mr-4">
                            <input type="email" class="form-control" name="Email" placeholder="email" required/>
                        </div>
                        <div class="col-md m-0 ml-4">
                            <input type="tel" class="form-control" name="Telephone" placeholder="+380" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col w-100 text-center">
                            <input type="submit" class="btn btn-success mx-auto" name="Send" value="Отправить"/>
                        </div>
                    </div>
                    <!-- user input section end -->
                </form>
            </div>
        </div>
        <!-- thank you alert section start -->
        <a hidden class="thank-you-alert-button"></a>
        <div class="mfp-hide text-center thank-you-alert-box position-relative mx-auto">
            <h5>
                Ваш кейс успішно відправленній на розгляд та незабаром буде розміщений в каталозі
            </h5>
        </div>
        <!-- thank you alert section end -->
        <!-- popup form section end -->
        <!-- nav section start -->
        <div class="row">
            <div class="col-md p-0 m-0 mr-2 ml-2 w-100">
                <nav class="nav nav-pills cases-nav mb-1">
                    <a class="nav-item nav-link my-auto p-0" href="#">
                        <span>Назва компанії</span>
                        <svg class="polygon polygon-top" fill="#2D2D2D" viewBox="0 0 15 13" xmlns="http://www.w3.org/2000/svg">
                              <polygon points="0,13 7.5,0 15,13" />
                        </svg>
                    </a>
                    <div class="nav-item my-auto">
                        <button id="year-selector" type="button" class="btn btn-sm">
                            <span>Дата</span>
                            <svg class="polygon polygon-top" fill="#2D2D2D" viewBox="0 0 15 13" xmlns="http://www.w3.org/2000/svg">
                                <polygon points="0,13 7.5,0 15,13" />
                            </svg>
                        </button>
                    </div>
                    <a class="nav-item nav-link my-auto p-0" href="#">
                        <span>Бюджет</span>
                        <svg class="polygon polygon-top" fill="#2D2D2D" viewBox="0 0 15 13" xmlns="http://www.w3.org/2000/svg">
                              <polygon points="0,13 7.5,0 15,13" />
                        </svg>
                    </a>
                </nav>
                <div class="accordion content" id="accordionExample">
                    @forelse($posts as $post)
                        <div class="postcard card border-0 m-0 p-0" data-date="{{date('Y', strtotime($post->updated_at))}}" data-name="{{$post->company_name}}" data-budget="{{$post->budget}}">
                            <div class="card-header bg-white border-0 pt-0 pb-0" id="heading{{$post->id}}">
                                <h2 class="mb-0">
                                    <button type="button" class="btn btn-link btn-block text-left m-0 p-0" data-toggle="collapse" data-target="#collapse{{$post->id}}" aria-expanded="true" aria-controls="collapse{{$post->id}}">
                                        <span class="row m-0 p-0 w-100">
                                            <span class="col ml-3 p-0 m-0 ml-4">
                                                <svg class="polygon polygon-side" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                                      <polygon points="0,0 0,100 100,50" />
                                                </svg>
                                                <span class="name">{{$post->company_name}}</span>
                                            </span>
                                            <span class="col text-center p-0 m-0 pr-3">
                                                <span>
                                                    {{date('Y', strtotime($post->created_at))}} - {{date('Y', strtotime($post->updated_at))}}
                                                </span>
                                            </span>
                                            <span class="col text-right p-0 m-0 mr-4">
                                                <span>{{ $post->budget }}грн.</span>
                                            </span>
                                        </span>
                                    </button>
                                </h2>
                                <hr class="line"/>
                            </div>
                            <div id="collapse{{$post->id}}" class="collapse" aria-labelledby="heading{{$post->id}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    <h3 class="pl-4">
                                        <span>{{$post->project_name}}</span>
                                    </h3>
                                    <div class="card-body-content">
                                        <div class="row w-100">
                                            @if($post->image[0] != '')
                                                <div class="col-md">
                                                    <div id="carousel{{$post->id}}" class="carousel slide" data-ride="carousel">
                                                        <div class="carousel-inner">
                                                            @foreach($post->image as $img_k => $img_v)
                                                                @if($img_k == 0)
                                                                    <div class="carousel-item active">
                                                                        <img class="d-block w-100" src="{{ asset('storage'.DIRECTORY_SEPARATOR.$img_v) }}" alt="picture">
                                                                    </div>
                                                                @else
                                                                    <div class="carousel-item">
                                                                        <img class="d-block w-100" src="{{ asset('storage'.DIRECTORY_SEPARATOR.$img_v) }}" alt="picture">
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <a class="carousel-control-prev" href="#carousel{{$post->id}}" role="button" data-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="#carousel{{$post->id}}" role="button" data-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            @else
                                            @endif
                                            <div class="col-md">
                                                    <span>
                                                        <?php echo $post->body; ?>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body-links pl-4">
                                        <?php
                                        echo $post->links;
                                        ?>
                                    </div>
                                </div>
                                <hr class="line"/>
                            </div>
                        </div>
                    @empty
                        <div class="row w-100 p-0 m-0">
                            <div class="col p-0 m-0 text-center">
                                <h1>На данний момент каталог пустий</h1>
                            </div>
                        </div>
                    @endforelse
                    <nav id="pagination" class="justify-content-center px-4" aria-label="Page pagination example" style="justify-content: center">
                        {{ $posts->links() }}
                    </nav>
                </div>
            </div>
        </div>
        <!-- nav section end -->
    </div>
@endsection
