<!-- scripts section start -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="{{ asset('assets/libs/magnific-popup/jquery.magnific-popup.js') }}"></script>
<script lang="text/javascript" src="https://unpkg.com/@popperjs/core@2" defer></script>
<script lang="text/javascript" src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}" defer></script>
<script lang="text/javascript" src="{{ asset('assets/js/form.js') }}" defer></script>
<script lang="text/javascript" src="{{ asset('assets/js/main.js') }}" defer></script>
<!-- scripts section end -->
