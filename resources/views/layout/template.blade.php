<!doctype html>
<html lang="ua">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Каталог</title>
        @include('layout.css')
    </head>
    <body>
        <!-- errors section start -->
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- errors section end -->
        @yield('content')
        @include('layout.scripts')
    </body>
</html>
