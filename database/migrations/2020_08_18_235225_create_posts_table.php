<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('author', 100);
			$table->integer('category_id')->nullable();
			$table->string('project_name');
			$table->string('company_name')->nullable();
			$table->text('links', 65535)->nullable();
			$table->text('body', 65535);
			$table->string('image')->nullable();
			$table->text('meta_description', 65535)->nullable();
			$table->text('meta_keywords', 65535)->nullable();
			$table->string('status');
			$table->boolean('featured')->default(0);
			$table->timestamps();
			$table->integer('budget');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
