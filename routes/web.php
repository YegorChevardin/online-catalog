<?php

Route::get('/', 'EcoController@index')->name('eco');
Route::get('/soc', 'SocController@index')->name('soc');

Route::post('/post', 'PostController@index')->name('post');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
