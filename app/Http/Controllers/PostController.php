<?php

namespace App\Http\Controllers;

use App\Mail\PostMail;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{
    function index(Request $request) {
        if(!empty($request)) {

            //validation
            $data = $request->validate([
                'Company_name' => 'required|max:30',
                'Project_name' => 'required|max:100',
                'Budget' => 'required|max:11',
                'Description' => 'required|max:10000|min:30',
                'Links' => 'max:255',
                'Full_name' => 'required|max:100|'
            ]);

            //inserting info into database
            $post = new Post();
            $post->author = $request->input('Full_name');
            if($request->input('form_subject') == "Eco post") {
                $post->category_id = 3;
            } elseif($request->input('form_subject') == "Soc post") {
                $post->category_id = 4;
            } else {
                abort(404);
            }
            $post->project_name = $request->input('Project_name');
            $post->company_name = $request->input('Company_name');
            $post->links = $request->input('Links');
            $post->body = $request->input('Description');
            $post->status = 'pending';
            $post->featured = 0;
            $post->created_at = $request->input('Start_date');
            $post->updated_at = $request->input('End_date');
            $post->budget = $request->input('Budget');
            $post->save();

            //sending email
            $mail_data = [
                'ФИО' => $request->input('Full_name'),
                'Email' => $request->input('Email'),
                'Телефон' => $request->input('Telephone'),
                'Тип посту' => $request->input('form_subject'),
                'Назва компанії' => $request->input('Company_name'),
                'Назва проекту' => $request->input('Project_name'),
                'Бюджет' => $request->input('Budget'),
                'Час' => "З ".$request->input('Start_date')." по ".$request->input('End_date')
            ];
            Mail::to(['onlineecocatalog@gmail.com', 'maisterniamista.nik@gmail.com', 'ies.info.org@gmail.com'])->send(new PostMail($mail_data));

            return redirect()->back();
        }
        abort(404);
    }
}
