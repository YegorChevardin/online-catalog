<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SocController extends Controller
{
    function index() {
        if(View::exists('soc-catalog')) {
            $posts = Post::where('category_id', 4)->where('status', 'published')->paginate(10);

            /* getting array from images */
            foreach ($posts as $post) {
                $post->image = trim($post->image, '[]"');
                $post->image = explode('","', $post->image);
            }

            return view('soc-catalog')->with([
                'posts' => $posts
            ]);
        } else {
            abort(404);
        }
    }
}
