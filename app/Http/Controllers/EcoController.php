<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Post;
use App\Category;

class EcoController extends Controller
{
    function index() {
        if(View::exists('eco-catalog')) {
            $posts = Post::where('category_id', 3)->where('status', 'published')->paginate(10);

            /* getting array from images */
            foreach ($posts as $post) {
                $post->image = trim($post->image, '[]"');
                $post->image = explode('","', $post->image);
            }

            return view('eco-catalog')->with([
                'posts' => $posts
            ]);
        } else {
            abort(404);
        }
    }
}
